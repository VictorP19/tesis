# Terminales para correr el código 

Bashear sb y so (son alias de source bashrc y source el dlevel del ws) en cada terminal

### T1:

roscore 

### T2:

(opcional si se hace Real time desde la cámara y se va a usar la imu)

`roslaunch realsense2_camera rs_camera.launch align_depth:=true unite_imu_method:="linear_interpolation" enable_gyro:=true enable_accel:=true`

### T3: 

(Si se hace real time desde la camara)

`rosrun imu_filter_madgwick imu_filter_node _use_mag:=false _publish_tf:=false _world_frame:="enu" /imu/data_raw:=/camera/imu /imu/data:=/rtabmap/imu`

### T4: 

(si se hace desde el rosbag, cd ws_otro)

`python republish_tf_static.py `

### T5: 

(Si se hace desde rosbag)
`rosbag play --clock -s 0 cortado.bag /tf_static:=tf_static_old ` 

(verificar si se quiere empezar desde el segundo 0 o no) es el del dia 19

### T6:

`roslaunch rgbdOPT.launch  rtabmap_args:="--delete_db_on_start"` (hacer cd a ws_otro, chequear en rtabmapviz los 2 parametros para visualizar el grid)
**(verificar si se queiere mapear o localizar)**



T extra: ir chequeando rosnodes y rostopics